#!/usr/bin/python

import optparse
import sys


USAGE = """%prog SDK_PATH FORM_NAME
Build clients form using ngforms.

SDK_PATH    Path to the SDK installation
FORM_NAME   Form's name"""


def main(sdk_path, form_name):
    sys.path.insert(0, sdk_path)
    import dev_appserver
    dev_appserver.fix_sys_path()

    import webapp2
    cls = webapp2.import_string('dymotics.forms.%s' % form_name)
    f = cls()
    print f.build()


if __name__ == '__main__':
    parser = optparse.OptionParser(USAGE)
    options, args = parser.parse_args()
    if len(args) != 2:
        print 'Error: Exactly 2 arguments required.'
        parser.print_help()
        sys.exit(1)

    SDK_PATH = args[0]
    FORM_NAME = args[1]
    main(SDK_PATH, FORM_NAME)

