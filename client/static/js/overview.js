'use strict';



$(function() {

  $.fn.serializeObject = function(){
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
  };

  $('#create-contract-button').click(function(e) {
    e.preventDefault();

    var fdata = {
      contractTitle: $('#contractTitle').val(),
      contractModel: $('#contractModel').val(),
    };

    var stack_bottomright = {
      addpos2: 0,
      animation: true,
      dir1: "up",
      dir2: "left",
      firstpos1: 25,
      firstpos2: 25,
      nextpos1: 25,
      nextpos2: 25,
    };

    $.ajax({
      type: 'POST',
      url: '/_/contracts/add',
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      data: JSON.stringify(fdata),
      dataFilter: function(data) {
        data = data.replace(')]}\',', '');
        return data;
      }
    }).done(function(data) {
      if(data.error == '') {
        $.pnotify({
          title: 'Contract added!',
          text: 'You can start to manage right now:P',
          mouse_reset: false,
          type: 'success',
          stack: stack_bottomright,
          delay: 2500,
        });
      }else if(data.error == '') {
        console.log('all not was ok!');
      }
    }).fail(function(data) {
      console.log('all not was ok!');
      console.log(data);
    });
  });
  
}); 
