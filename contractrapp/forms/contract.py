
from gaelib.ngforms import *


class AddContractForm(Form):
  @property
  def fields(self):
    return [
      InputField(id='contractTitle', name='contractTitle', placeholder='',
          cls=['input-xlarge']),
      InputField(id='contractModel', name='contractModel', placeholder='',
          cls=['input-xlarge']),
      SubmitField(label='Create'),
    ]

  @property
  def validations(self):
    return {
      'contractTitle': [
        Required('The contract title is required'),
      ],
      'contractModel': [
        Required('The contract model is required'),
      ],
    }
