
from google.appengine.ext import ndb


class Contract(ndb.Model):
  StartTime = ndb.StringProperty(indexed=False)
  EndTime = ndb.StringProperty(indexed=False)
  Status = ndb.StringProperty(indexed=False)
  User = ndb.StringProperty()
  Name = ndb.StringProperty(indexed=False)
  Model = ndb.StringProperty(indexed=False)

  @classmethod
  def return_all_contracts_from_user(cls, email):
    models = Contract.query(Contract.User == email)
    objs = []
    for model in models:
      objs.append({'ID': model.key, 'StartTime': model.StartTime,
          'EndTime': model.EndTime, 'Status': model.Status, 
          'Name': model.Name, 'Model' : model.Model})

    if not objs:
      return None

    return objs
