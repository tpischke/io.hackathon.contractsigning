
from gaelib import handlers

from google.appengine.api import users

from contractrapp.models.contract import Contract


# BaseHandler which loads the main page through GET request to /
class BaseHandler(handlers.Base):
  def get(self):
    if users.get_current_user() is None:
      self.render('home.html', login_url=users.create_login_url('/'))
    else:
      all_contracts = Contract.return_all_contracts_from_user(users.get_current_user().email())
      all_contracts_empty = True
      if not all_contracts is None:
        all_contracts_empty = False

      self.render('overview.html', logout_url=users.create_logout_url('/'),
          nickname=users.get_current_user().email(), contracts=all_contracts,
          contracts_empty=all_contracts_empty)
