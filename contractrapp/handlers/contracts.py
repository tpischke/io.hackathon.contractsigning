
from gaelib import handlers

from contractrapp.models.contract import Contract
from contractrapp.forms.contract import AddContractForm

from google.appengine.api import users


class AddContractHandlerHandler(handlers.Base):
  def post(self):
    data = AddContractForm().validate()

    print data
    new_contract = Contract(User=users.get_current_user().email(), 
        Name=data['contractTitle'], Model=data['contractModel'], Status='ok', 
        StartTime='Today', EndTime='Tomorrow')
    new_contract.put();

    self.json({'error': ''})
